#!/usr/bin/env bash

#-------------------------------------------------------------------------

echo
echo "FINAL CONFIGURATION"

# ------------------------------------------------------------------------

echo
echo "Increasing file watcher count"

# This prevents a "too many files" error in Visual Studio Code
echo fs.inotify.max_user_watches=524288 | sudo tee /etc/sysctl.d/40-max-user-watches.conf && sudo sysctl --system

# ------------------------------------------------------------------------

echo
echo "Enabling Network Time Protocol so clock will be set via the network"

sudo ntpd -qg
sudo systemctl enable ntpd.service
sudo systemctl start ntpd.service

# ------------------------------------------------------------------------

echo
echo "Enabling HW Acceleration"

unset LIBVA_DRIVER_NAME VDPAU_DRIVER DRI_PRIME

if lspci -k | grep -q -e amdgpu -e radeon; then
	export LIBVA_DRIVER_NAME=radeonsi
	export VDPAU_DRIVER=radeonsi
fi

# ------------------------------------------------------------------------

echo
echo "Setting up fonts"

sudo echo `export FREETYPE_PROPERTIES="truetype:interpreter-version=40"` >| /etc/profile.d/freetype2.sh

sudo tee ~/.config/fontconfig/conf.d/20-no-embedded.conf >/dev/null << EOF
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
  <match target="font">
    <edit name="embeddedbitmap" mode="assign">
      <bool>false</bool>
    </edit>
  </match>
</fontconfig>
EOF

sudo tee ~/.config/fontconfig/fonts.conf >/dev/null << EOF
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>

    <match target="pattern">
        <test qual="any" name="family"><string>monospace</string></test>
        <edit name="family" mode="append" binding="weak"><string>Apple Color Emoji</string></edit>
    </match>
    <match target="pattern">
        <test qual="any" name="family"><string>serif</string></test>
        <edit name="family" mode="append" binding="strong"><string>Apple Color Emoji</string></edit>
    </match>
    <match target="pattern">
        <test qual="any" name="family"><string>sans-serif</string></test>
        <edit name="family" mode="append" binding="strong"><string>Apple Color Emoji</string></edit>
    </match>

</fontconfig>
EOF

echo
echo "Disabling root user"

sudo passwd --lock root

# ------------------------------------------------------------------------

echo
echo "Enabling System Services"

sudo systemctl enable docker.service
sudo systemctl enable sddm.service

clear
echo
echo "Reboot now..."
sleep 3
reboot
